# Global Internet public peering capacity of interconnection a complex network analysis

Source code of arXiv:2206.05146 "Global Internet public peering capacity of interconnection: a complex network analysis"

Loye, J., Mouysset, S., Bruyère, M., & Jaffrès-Runser, K. (2022). Global Internet public peering capacity of interconnection: a complex network analysis. arXiv preprint arXiv:2206.05146.
