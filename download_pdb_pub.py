# For each caida.org/datasets/peeringdb/ directory, we load
# the file md5.md5 that contains the list of snapshot files we need to read.
# note that there is a gap from 2018-03-11 to 2018-03-31 in the snapshot files that is
# still listed in md5.md5. I removed them manually in caida_pdbs/files_list.txt after the script execution

import requests
from pathlib import Path

Path("./caida_pdbs/download").mkdir(parents=True, exist_ok=True)

years = [str(i) for i in range(2010,2022)]
months = ["01","02","03","04","05","06","07","08","09","10","11","12"]

# def filter_mdm5(mdm5):
#     ##v1 case
#     if year <


files_list_all = [] #list of file for all snapshots
for year in years:
    for month in months:
        path = "http://data.caida.org/datasets/peeringdb/"+year+"/"+month+"/"
        print("Reading files in", path)

        try:
            r = requests.get(path+"md5.md5")   

            files_list = [] #list of snapshots for this month
            for line in r.text.splitlines():
                if len(line)>1:
                    files_list.append(line.split()[0])

            files_list = [element for element in files_list if (".sqlite" in element) or (".json" in element)] ##sql and sqlit snapshots coexist, while json is always the only version
            files_list = [element for element in files_list if not "peeringdb_3_dump" in element] ##we do not need duplicated peeringdb_3 data

            for filename in files_list:
                r = requests.get(path+filename)
                open('caida_pdbs/download/'+filename, 'wb').write(r.content)
                files_list_all.append(filename)
        except:
            print("out of range")
        print(files_list_all)

files_output = open("caida_pdbs/download/files_list.txt", "w")
for title in files_list_all:
    print(title, file=files_output)
#this last file need to be manually sanitize and copied in the caida_pdbs/download directory
